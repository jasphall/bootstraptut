<? /* Nawigacja + pasek górny */ ?>

<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">

    <!-- Naglowek paska nawigacyjnego -> lewa strona -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Rozwiń nawigację</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">UMK.tv</a>
      <p class="navbar-text">Telewizja uniwersytecka</p>
    </div>
    
    <!-- Naglowek paska nawigacyjnego -> środek -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Strona Główna</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Aplikacja<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Wszystkie filmy</a></li>
            <li class="divider"></li>
            <li><a href="#">Właściwa aplikacja</a></li>
          </ul>
        </li>
      </ul>

      <!-- Naglowek paska nawigacyjnego -> prawa strona (szukajka) -->
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Szukaj">
        </div>
        <button type="submit" class="btn btn-default">Wyślij</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Info</a></li>
      </ul>

    </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
</nav>